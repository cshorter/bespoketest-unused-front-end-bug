<!DOCTYPE html>
<html lang="en">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <title>Contact Us, Castus Web Design</title>

    <meta name="description"
          content="Say hello if you&#039;d like to discuss a web design project, join our team, find out more about us, or just to say hi."/>
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" type="text/css" href="https://www.castus.co.uk/site/assets/css/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="/css/app.css"/>


</head>
<body id="contact" class="loading">

<div id="wrap">
    <div id="slideNav">
        <div>
            <a href="#" class="burger toggleNav"><span></span><span></span><span></span></a>
            <a class="logo" href="/" alt="Logo">
                <span>Castus</span>
                <i class="logoImg icon logo_white"></i>
                <i class="logoImg icon logo_small_white"></i>
            </a>
            <nav>
                <ul>
                    <li><a href="https://www.castus.co.uk/">Home</a></li>
                    <li><a href="https://www.castus.co.uk/about-us/">About Us</a></li>
                    <li><a href="https://www.castus.co.uk/services/">Services</a></li>
                    <li><a href="https://www.castus.co.uk/portfolio/">Portfolio</a></li>
                    <li><a href="https://www.castus.co.uk/careers/">Careers</a></li>
                    <li><a href="/">Contact Us</a></li>
                </ul>
            </nav>

            <p class="contactDetails">0114 2211906<br>
                <a href="mailto:info@castus.co.uk">info@castus.co.uk</a></p>
            <address>Castus Ltd<br/>
                Cooper Buildings<br/>
                Sheffield Tech Parks<br/>
                Arundel Street<br/>
                Sheffield<br/>
                S1 2NS
            </address>
            <ul class="socialLinks">
                <li><a href="https://twitter.com/castusdesign" target="_blank" rel="noopener noreferrer">Twitter</a>
                </li>
                <li><a href="https://www.linkedin.com/company/castus-design" target="_blank" rel="noopener noreferrer">LinkedIn</a>
                </li>
                <li><a href="https://www.instagram.com/castusdesign" target="_blank"
                       rel="noopener noreferrer">Instagram</a></li>
                <li><a href="https://www.facebook.com/castusdesign/" target="_blank"
                       rel="noopener noreferrer">Facebook</a></li>
            </ul>
        </div>
    </div>


    <header id="header">
        <a href="#" class="burger toggleNav"><span></span><span></span><span></span></a>
        <div class="logoWrap">
            <a href="/">
                <span>Castus</span>
                <i class="logoImg icon logo"></i>
                <i class="logoImg icon logo_small"></i>
            </a>
            <div class="logoSidebarTrigger" data-sidebar="0,1"></div>
        </div>

        <a href="#" class="burger toggleNav"><span></span><span></span><span></span></a>
        <nav id="mainNav">
            <ul>
                <li><a href="https://www.castus.co.uk/">Home</a></li>
                <li><a href="https://www.castus.co.uk/about-us/">About Us</a></li>
                <li><a href="https://www.castus.co.uk/services/">Services</a></li>
                <li><a href="https://www.castus.co.uk/portfolio/">Portfolio</a></li>
                <li><a href="https://www.castus.co.uk/careers/">Careers</a></li>
                <li><a href="/">Contact Us</a>
                    <div class="menuContact">
                        <p>0114 2211906<br>
                            <a href="mailto:info@castus.co.uk">info@castus.co.uk</a></p>
                    </div>
                </li>
            </ul>
        </nav>
        <div class="pageTitle"><h1>Contact Us</h1></div>
        @if (!session('message'))
        <div class="intro">
            <p>We&#039;d love to hear from you.<br/>
                Say hello with the form below, or give us a call or email.</p></div>
        @endif

    </header>

    <main>

        @if (session('message'))
        <div class="thanks">
            <div class="content">
                <div class="content__intro">
                    <p>Thank you for getting in touch. <br/>
                        We'll get back to you in no time.</p>
                </div>
            </div>
        </div>
        @else

        <div class="form">
            <form method="POST" action="/enquiries/store">

                @csrf

                <div class="form__inner">
                    <p class="form__field">
                        <label for="name">Name</label>
                        <input type="text" id="name" name="name" value="" required>
                    </p>
                    <p class="form__field">
                        <label for="email">Email</label>
                        <input type="email" id="email" name="email" value=""
                               pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$"
                               required>
                    </p>
                    <p class="form__field">
                        <label for="phone">Phone</label>
                        <input type="tel" id="phone" name="phone" value="">
                    </p>
                    <p class="form__field">
                        <label for="body">Message</label>
                        <textarea name="body" id="body" required></textarea>
                    </p>
                    @if($errors->any())
                    <div class="errors">
                        <p>We're really sorry, but something went wrong with that submission. Would you mind checking
                            over your details again and giving it another shot?</p>
                    </div>
                    @endif
                    <p class="form__submit">
                        <button class="submit arrowLink">
                            Send enquiry
                            <span class="arrowWrap">
                                <i class="icon arrow-large-grey"></i>
                            </span>
                        </button>
                    </p>
                </div>
            </form>
        </div>

        @endif

        <div class="contactDetails">
            <div class="contactGroup">
                <h4>Phone</h4>
                <p>0114 2211906</p>
                <h4>General Email</h4>
                <p>
                    <a href="mailto:info@castus.co.uk" class="arrowLink">
                        info@castus.co.uk <span class="arrowWrap">
                    <i class="icon arrow-large-grey"></i>
                </span>
                    </a>
                </p>
            </div>
            <div class="contactGroup">
                <h4>Address</h4>
                <address>
                    Castus Ltd<br/>
                    Cooper Buildings<br/>
                    Sheffield Tech Parks<br/>
                    Arundel Street<br/>
                    Sheffield<br/>
                    S1 2NS
                </address>
            </div>
            <div class="contactGroup">
                <h4>Social</h4>
                <ul>
                    <li><a href="https://twitter.com/castusdesign" target="_blank" rel="noopener noreferrer">Twitter</a>
                    </li>
                    <li><a href="https://www.linkedin.com/company/castus-design" target="_blank"
                           rel="noopener noreferrer">LinkedIn</a></li>
                    <li><a href="https://www.instagram.com/castusdesign" target="_blank" rel="noopener noreferrer">Instagram</a>
                    </li>
                    <li><a href="https://www.facebook.com/castusdesign/" target="_blank" rel="noopener noreferrer">Facebook</a>
                    </li>
                </ul>
            </div>
        </div>


        <iframe class="map" width="100%" height="699" frameborder="0" style="border:0"
                src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJp1icD_qfeUgRzD-CDu4IL2Y&key=AIzaSyC-as2uy3mkWoKopl26YPEbuFA4Dw8RA_o"
                allowfullscreen></iframe>
    </main>

    <div id="sidebar" class="sidebar state0 burger0">
        <a class="sideLogo" href="/">
            <span>Castus</span>
            <i class="logoImg icon logo_sidebar"></i>
            <i class="logoImg icon logo_sidebar_white"></i>
        </a>
        <a class="sideBurger toggleNav" href="#"><span></span><span></span><span></span></a>
        <a class="sideClose toggleNav" href="#"><span></span><span></span></a>
    </div>


    <footer>
        <div class="logo">
            <a href="/"><span>Castus</span><i class="icon logo_small"></i></a>
        </div>
        <div class="emailPhone">
            <p>0114 2211906<br>
                <a href="mailto:info@castus.co.uk">info@castus.co.uk</a></p>
        </div>
        <address>
            <p>Castus Ltd<br/>
                Cooper Buildings<br/>
                Sheffield Tech Parks<br/>
                Arundel Street<br/>
                Sheffield<br/>
                S1 2NS</p>
        </address>
        <div class="social">
            <ul class="socialLinks">
                <li><a href="https://twitter.com/castusdesign" target="_blank" rel="noopener noreferrer">Twitter</a>
                </li>
                <li><a href="https://www.linkedin.com/company/castus-design" target="_blank" rel="noopener noreferrer">LinkedIn</a>
                </li>
                <li><a href="https://www.instagram.com/castusdesign" target="_blank"
                       rel="noopener noreferrer">Instagram</a></li>
                <li><a href="https://www.facebook.com/castusdesign/" target="_blank"
                       rel="noopener noreferrer">Facebook</a></li>
            </ul>
        </div>
        <nav>
            <ul>
                <li><a href="/information/terms-of-service/">Terms of Service</a></li>
                <li><a href="/information/privacy-and-cookie-policy/">Privacy &amp; Cookie Policy</a></li>
            </ul>
        </nav>
        <p class="copyright">Copyright © 2018 Castus Ltd</p>
    </footer>

</div>

<script src='/js/app.min.js'></script>
</body>
</html>